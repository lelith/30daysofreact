import React, { Component } from "react";
import Formatter from "./Formatter.js"
import "./clock.css"

class Clock extends Component {

  constructor(props){
    super(props);
    this.state = this.getTime();
  }

  componentDidMount() {
    this.setTimer();
  }

  componentWillUnmount() {
    if (this.timeout){
      clearTimeout(this.timeout);
    }
  }

  setTimer() {
    setTimeout(this.updateClock.bind(this), 1000);
  }

  updateClock() {
    this.setState(this.getTime, this.setTimer);
  }

  getTime() {
    const currentTime = new Date();
    return {
      hours : currentTime.getHours(),
      minutes : currentTime.getMinutes(),
      seconds : currentTime.getSeconds(),
      ampm : currentTime.getHours() >= 12 ? "pm" : "am"
    }
  }

  render() {
    const {hours, minutes, seconds, ampm} = this.state;
    return (
      <div className="clock">
        <Formatter
          {...this.props}
          state = {this.state}
          hours={hours}
          minutes={minutes}
          seconds={seconds}
        />
      </div>
    )

  }
}

export default Clock
