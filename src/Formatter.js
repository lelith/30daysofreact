import React, { Component } from "react";

const Hour = (props) => {
  let {hours} = props;
  if (hours == 0 ) {
    hours = 12;
   }

   if(props.twelveHours) {
     hours -= 12;
   }

   return (<span className="hours">{hours}</span>)
}

const Minute = ({minutes}) => (
  <span className="minutes">{minutes<10 &&
  "0"}{minutes}</span>
)

const Second = ({seconds}) => (
  <span className="seconds">{seconds<10 &&
  "0"}{seconds}</span>
)

const Seperator = ({seperator}) => (
  <span className="seperator">{seperator || ":" }</span>
)

const Ampm = ({hours}) => (
  <span className="ampm">{hours >12 ? "pm" : "am" }</span>
)

const Formatter = ({format, state}) => {
  let children = format.split('').map(e => {
    if (e === 'h') {
      return <Hour />
    } else if (e === 'm') {
      return <Minute />
    } else if (e === 's') {
      return <Second  />
    } else if (e === 'p') {
      return <Ampm />
    } else if (e === ' ') {
      return <span> </span>;
    } else {
      return <Seperator />
    }
  });

  return(
    <span>{
      React.Children.map(children, c => React.cloneElement(c,state))
    }</span>
  )
}

export default Formatter
