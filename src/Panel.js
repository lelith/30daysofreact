import React, { Component } from "react"
import Header from './Header';
import Footer from './Footer';
import Content from './Content';
import Clock from './Clock';

class Panel extends Component {

  constructor(props) {
    super(props);

    this.state = {
      refreshing: false,
      searchFilter: ""
    }
  }

  refresh() {
    this.setState({refreshing: true})
  }

  onComponentRefresh() {
    this.setState({refreshing: false})
  }

  handleSearch(val) {
    this.setState({
      searchFilter: val,
      refreshing: true
    });
  }

  render() {

    return (
      <div className="panel1">

        <Header
          onSearch = {this.handleSearch.bind(this)}
          title="Github activity"
        />

        <Clock format="h:m:s p" />

      <Content
          onComponentRefresh = {this.onComponentRefresh.bind(this)}
          requestRefresh = {this.state.refreshing}
          searchFilter = {this.state.searchFilter}
      />

        <Footer>
          <button onClick={this.refresh.bind(this)}>
            <i className="fa fa-refresh" />
            Refresh
          </button>
        </Footer>
      </div>
    );
  }
}
export default Panel;
