import React, { Component } from "react";
import SearchForm from "./SearchForm.js"
import "./Header.css"

class Header extends Component {
  constructor (props) {
    super(props);

    this.state = {
      searchVisible: false
    }
  }

  showSearch() {
    this.setState({
      searchVisible: !this.state.searchVisible
    })
  }

  handleSearch(val) {
    this.props.onSearch(val);
  }

  render() {


    return (
      <div className = "header">
        <h1 className = "title">
          {this.props.title}
        </h1>

        <div className = "search">
          <i
            onClick = {this.showSearch.bind(this)}
            className = "fa fa-search searchIcon">
          </i>
          <SearchForm
            searchVisible = {this.state.searchVisible}
            onSubmit = {this.handleSearch.bind(this)}
          />
        </div>
      </div>
    );
  }
}

Header.propTypes = {
  title: React.PropTypes.string,
  onSearch: React.PropTypes.func
}

export default Header
