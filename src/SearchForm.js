import React, { Component } from "react";
import "./SearchForm.css"
class SearchForm extends Component {
  constructor(props) {
    super(props);

    this.state = {
      searchText: "",
    }
  }

  updateSearchInput(event) {
    this.setState({
      searchText: event.target.value
    });
  }

  submitForm(event) {
    event.preventDefault();

    //bubble up the search text we safe in the state
    this.props.onSubmit(this.state.searchText);
  }

  render() {
    let searchInputClasses = ["searchForm"];

    if(this.props.searchVisible){
      searchInputClasses.push("active");
    }

    return (
      <form
        className = {searchInputClasses.join(" ")}
        onSubmit={this.submitForm.bind(this)}
      >
        <input
          type = "text"
          className = "searchForm-input"
          onChange = {this.updateSearchInput.bind(this)}
          placeholder = "Search..."
        />
      </form>
    );
  }
}

SearchForm.propTypes = {
  searchVisible: React.PropTypes.bool,
  onSubmit: React.PropTypes.func.isRequired
}

SearchForm.defaultProps = {
  searchVisible: false,
  onSubmit: () => {} // empty function defintion
}

export default SearchForm
