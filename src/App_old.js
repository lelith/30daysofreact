import React, { Component } from "react"
import "whatwg-fetch"
import Panel from "./Panel"
import TimeForm from "./TimeForm"

class App_old extends Component {

  constructor(props) {
    super (props);

    this.state = {
       currentTime: null, msg: 'now', tz: 'PST'
    }
  }

  getApiUrl() {
      const {tz, msg} = this.state;
      const host = "https://fullstacktime.herokuapp.com";
      return `${host}/${tz}/${msg}.json`;
  }

  fetchCurrentTime() {
    fetch(this.getApiUrl())
      .then(response => response.json())
      .then(response => {
        const currentTime = response.dateString;
        this.setState({currentTime})
      })
  }

  handleFormSubmit(evt) {
    this.fetchCurrentTime();
  }

  handleChange(newState) {
    this.setState(newState);
  }

  render() {
    const {currentTime, tz} = this.state;
    const apiUrl = this.getApiUrl();
    return (

      <div className="notificationsFrame">
        <Panel />
        {!currentTime &&
          <button onClick={this.fetchCurrentTime.bind(this)}>
            get Time
          </button>
        }
        {currentTime && <div>the current Time is : {currentTime}</div>}
        <TimeForm
          onFormSubmit = {this.handleFormSubmit.bind(this)}
          onFormChange = {this.handleChange.bind(this)}
          tz={tz}
          msg = {"now"}
        />
      <p>We ll make a request from: {apiUrl}</p>
      </div>
    );
  }
}

export default App_old;
