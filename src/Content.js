import React, { Component } from 'react';
import ActivityItem from './ActivityItem';

const data = require('./data.json').slice(0, 4)

class Content extends Component {

  constructor(props) {
    super(props);

    this.state = {
      loading: false, // <~ set loading to false
      activities: []
    }
  }

  componentDidMount() {
    this.updateData();
  }

  componentWillReceiveProps(nextProps){
    if(nextProps.requestRefresh !== this.props.requestRefresh){
      this.setState({loading:true}, this.updateData);
    }
  }

  updateData() {
    var result;
    if (this.props.searchFilter !=="") {
      result = data.filter((e => e.actor.login.match(new RegExp(this.props.searchFilter))));
    } else {
      result = data.sort(() => 0.5 - Math.random()).slice(0, 4)
    }

    this.setState({
      loading: false,
      activities: result
    }, this.props.onComponentRefresh);
  }

  render() {
    const {loading, activities} = this.state;

    return (
      <div className="content">
        <div className="line"></div>
        {loading && <div>Loading</div>}
        {/*Timeline item*/}
        {activities.map((activity) => (
          <ActivityItem
            key={activity.id}
            activity={activity} />
        ))}
      </div>
    )
  }
}
export default Content
